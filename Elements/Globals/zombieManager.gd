extends Node

var lights = false

var frame = 0

var spawnTimes = 15.0
var nextSpawn = spawnTimes

var difficultyNum = 100.0
var difficulty = 5.0

var zList:Array = []


var zombieLimit = 200

func _ready():
	spawn()


func _process(delta):
	pass
	
func updatePaths(zList):
	frame += 1
	zList.sort_custom(self,"sortPaths")
	var index = 0
	var startTime = OS.get_system_time_msecs()
	while (startTime + 1) > OS.get_system_time_msecs() and index < zList.size():
		var z = zList[index]
		index += 1
		z.updatePath()
	debug.report("zManger", { pathsUpdated = index, zombies = zList.size() })
	
	
	
func _physics_process(delta):
	zList = get_tree().get_nodes_in_group("zombieController")
#	print(zList.size())
	updatePaths(zList)
	trySpawn(delta)
	
func trySpawn(delta):
	if nextSpawn < 0 and zList.size() < zombieLimit:
		nextSpawn += (difficultyNum / difficulty)
		print('next cluseter in :',nextSpawn, "sec")
		spawn()
	else:
		nextSpawn -= delta

	
func spawn():
	var spawners = get_tree().get_nodes_in_group("ZSpawner")
	player = get_tree().get_nodes_in_group("player")[0]
	
	spawners.sort_custom(self,"sortSpawners")
	
	var spawnGroup = spawners[randi() % (spawners.size() - 1) + 1]
	var spawnPoint = spawnGroup.get_child(randi() % spawnGroup.get_children().size())
	
	var zCluster:Node2D = preload("res://Elements/GameElements/ZCluster.tscn").instance()
	spawnPoint.add_child(zCluster)
	zCluster.global_scale = Vector2.ONE



var player:Node2D#s = []
func sortSpawners(a,b):
	return player.global_position.distance_squared_to(a.global_position) < player.global_position.distance_squared_to(b.global_position)
	


func sortPaths(a,b):
	return a.pathAge < b.pathAge