extends Node

var lights = true
var debugView = false

func _ready():
	for i in get_tree().get_nodes_in_group("light"):
		i.visible = lights
	
var reports = {}

func _process(delta):
	report("FPS",Performance.get_monitor(Performance.TIME_FPS))
	#todo display reports
#	print("hit")
	if Input.is_action_just_pressed("toggleLight"):
		lights = !lights
		for i in get_tree().get_nodes_in_group("light"):
			i.visible = lights
	
	if Input.is_action_just_pressed("debug"):
		debugView = !debugView
		
	if debugView:
		renderDebugView()
		
func renderDebugView():
	print(reports)
	reports = {}

func report(label,value):
	reports[label] = value