# Zombie Shooter

This game is for a jam. It is a simple twin stick zombie shooter.

## Notes

Originally this game was an Isometric game. However, I couldn't get commitments
from artists, so I abandon that and as of creating this document. I want to note
this because this ahs become a source of frustration a existential dread. When I
finish this, I want to note this place in my mind and acknowledge I passed
through it.

I also want to note that I NEED to create a wide design document FIRST and not
after I believed the project is a failure.
 

## Overall Design

### Grid map

### Basic Pawn

Pawns are Kinematic Bodies. Pawn collision shape, controller, AI, sprite, and
everything else are managed by an child.

The pawn only dictates how the character moves in game and reports back
displacement and collisions.

- BasicPawn (KinematicBody2D): responds to movement requests

### Zombie

Movement intention is based on Boids. This is a flocking algorithm used to
simulate flocking birds. The rules are simple. Avoid collisions, avoid
separation, and try to match movement direction of neighbors. My version will
add a rule for path finding.

Zombies have health and can be placed in a "downed" state when they take damage.
Any "downed" zombies do not effect neighbor AI. Downed Zombies return to the
fight after a moment until they die.

- Zombie (BasicPawn)
    - Body (Collision Shape): Unique shape for this character
    - Neighborhood (Area2D): used for finding the closest zombies
    - Personal (Area2D): used for finding the too-close zombies
    - AI (Node) : Decides movement
    - Feeler (ray2d): used by AI to find obstacles
    - NavPoint (Node2D): used for path finding
    - Sprite (not sure who should control animation. maybe the AI?)

### Hero

Hero uses WASD to control and aims with the mouse. Clicks will fire the active
item. The players each have a gun and a melee weapon. Guns run out of ammo.
Heros have health.

- Hero (BasicPawn)
    - Control : manages the hero via movement and actions
        - Input : some client will own a node here for setting controls

#### Inventory

Heros have inventory. This includes a main gun, a pistol, and an active item
(health kit, grenade, etc).

#### Gun

Gun object defines these properties

- Projectile type
- Fire rate
- Auto vs semi
- Clip Size
- Reload time
- Spread
- Shots per fire


### Player

Each player has two unique objects. A control capture object and a UI object. UI
object reads that player's game state and displays the necessary UI. Usually
either "spectate" or "play".

The UI manages the camera and the input capture node.





